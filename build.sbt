name := "crawler"

version := "0.1"

scalaVersion := "2.13.4"


libraryDependencies ++= Seq(
  "io.circe" %% "circe-generic" % "0.13.0",
  "io.circe" %% "circe-core" % "0.13.0",
  "io.circe" %% "circe-parser" % "0.13.0",
  "org.jsoup" % "jsoup" % "1.13.1",

  "com.typesafe.akka" %% "akka-actor" % "2.6.10",
  "com.typesafe.akka" %% "akka-http" % "10.2.1",
  "de.heikoseeberger" %% "akka-http-circe" % "1.35.2",
  "com.typesafe.akka" %% "akka-stream" % "2.6.10",

  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "org.slf4j" % "slf4j-api" % "1.7.25",
  "ch.qos.logback" % "logback-classic" % "1.2.3",

  "com.h2database" % "h2" % "1.4.200",
  "com.typesafe.slick" %% "slick" % "3.3.3",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",

  "org.scalatest" %% "scalatest" % "3.2.0" % Test,
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.6.10" % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % "10.2.1" % Test,
  "org.scalamock" %% "scalamock" % "4.4.0" % Test
)