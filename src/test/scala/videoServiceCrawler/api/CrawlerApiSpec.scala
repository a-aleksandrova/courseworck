package videoServiceCrawler.api

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import coursework.videoServiceCrawler.CrawlerAPI
import coursework.videoServiceCrawler.crawler.CrawlerService
import coursework.videoServiceCrawler.data.{RequestVideoData, ResponseContent, VideoItem}
import coursework.videoServiceCrawler.exceptions.{CrawlerExceptionHandler, DataNotFoundException, ExceptionResponse, SearchErrorException}
import org.scalamock.scalatest.MockFactory
import org.scalatest.funspec.AnyFunSpec

import scala.concurrent.Future

class CrawlerApiSpec extends AnyFunSpec with ScalatestRouteTest with MockFactory {

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  describe("GET /crawler/search?title={filmTitle}&year={filmYear}") {
    it("возвращает список из ResponseContent c видео-сервисов на которых обнаружен требуемый фильм/сериал") {
      (mockGameService.search _)
        .expects(RequestVideoData("skubi-du", "2002")) //кириллицу не принимает
        .returns(Future.successful(Seq(responseContent)))

      Get("/crawler/search?title=skubi-du&year=2002") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[Seq[ResponseContent]] == Seq(responseContent))
      }
    }

    it("возвращает SearchErrorException, если фильм/сериал не найден ни на одном из видео-сервисов") {
      (mockGameService.search _)
        .expects(RequestVideoData("komnata", "2019"))
        .returns(Future.failed(SearchErrorException()))

      Get("/crawler/search?title=komnata&year=2019") ~> route ~> check {
        assert(status == StatusCodes.BadRequest)
        assert(responseAs[ExceptionResponse] ==  ExceptionResponse("No search results ):"))
      }
    }
  }

  describe("GET /crawler/storage/search?title={filmTitle}&year={filmYear}") {
    it("возвращает список из ResponseContent c видео-сервисов на которых обнаружен требуемый фильм/сериал") {
      (mockGameService.searchInStorage _) //иначе: Type mismatch. Required: RequestVideoData, found: (String, String) => RequestVideoData
        .expects(RequestVideoData("skubi-du", "2002"))
        .returns(Future.successful(Seq(responseContent)))

      Get("/crawler/storage/search?title=skubi-du&year=2002") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[Seq[ResponseContent]] == Seq(responseContent))
      }
    }

    it("возвращает SearchErrorException, если фильм/сериал не найден ни на одном из видео-сервисов") {
      (mockGameService.searchInStorage _)
        .expects(RequestVideoData("komnata", "2019"))
        .returns(Future.failed(DataNotFoundException("komnata_2019")))

      Get("/crawler/storage/search?title=komnata&year=2019") ~> route ~> check {
        assert(status == StatusCodes.BadRequest)
        assert(responseAs[ExceptionResponse] ==  ExceptionResponse("No data with specified requestID (komnata_2019)"))
      }
    }
  }


  private val mockGameService = mock[CrawlerService] //здесь ведь не нужен Generated mocks?..
  private val route =
    Route.seal(
      new CrawlerAPI(mockGameService).route
    )(exceptionHandler = CrawlerExceptionHandler.exceptionHandler)

  private val responseContent = ResponseContent(
    "skubi-du_2002",
    "MEGOGO",
    Seq(
      VideoItem(
        "skubi-du",
        "2002",
        "Покупка\nНавсегда: HD 349 руб, SD 249 руб.\nна 2 дня: HD 149 руб, SD 69 руб.",
        "src/test/resource/film-page/Скуби-Ду.html")
    )
  )
}