package videoServiceCrawler.crawler


import coursework.videoServiceCrawler.crawler.parsers.MegogoParser
import coursework.videoServiceCrawler.data.{ParseResult, RequestVideoData}
import java.io.File
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.funspec.AnyFunSpec


class MegogoParserTest extends AnyFunSpec with ScalaFutures {
  val Parser = new MegogoParser

  describe("searchVideo") {

    val htmlFile = new File("src/test/resource/search-page/Скуби-Ду.html")
    val searchPage: Document = Jsoup.parse(htmlFile, "UTF-8")

    it("если на странице поиска есть результат по запросу, возвращает ParseResult с извлеченными данными") {

      val data = new RequestVideoData("Скуби-Ду", "2002")
      val parseResult = Parser.searchVideo(searchPage, data)

      val expect =
        ParseResult(
          1,
          Seq("Скуби-Ду"),
          Seq("src/test/resource/film-page/Скуби-Ду.html"),
          Seq("src/test/resource/film-page/Скуби-Ду.html")
        )

      assert(parseResult.futureValue == expect)
    }

    it("если на странице поиска нет результата по запросу, возвращает пустой ParseResult") {

      val data = new RequestVideoData("Скуби-Ду", "1234")
      val parseResult = Parser.searchVideo(searchPage, data)

      val expect =
        ParseResult(0, Seq(), Seq(), Seq())

      assert(parseResult.futureValue == expect)
    }

  }

  describe("getConditions") {

    val htmlFile = new File("src/test/resource/film-page/Скуби-Ду.html")
    val filmPage: Document = Jsoup.parse(htmlFile, "UTF-8")

    it("возвращает текст условий просмотра одного фильма/сериала") {

      val parseResult = Parser.getConditions(filmPage)

      val expect = "Покупка\nНавсегда: HD 349 руб, SD 249 руб.\nна 2 дня: HD 149 руб, SD 69 руб."
      
      assert(parseResult.futureValue == expect)
    }
  }

}
