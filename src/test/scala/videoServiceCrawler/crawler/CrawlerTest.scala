package videoServiceCrawler.crawler

import coursework.videoServiceCrawler.crawler.Crawler
import coursework.videoServiceCrawler.crawler.clients.FileClient
import coursework.videoServiceCrawler.crawler.parsers.MegogoParser
import coursework.videoServiceCrawler.data.{RequestVideoData, ResponseContent, VideoItem}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.time.{Seconds, Span}

import scala.concurrent.ExecutionContext.Implicits.global

class CrawlerTest extends AnyFunSpec with ScalaFutures {

  val Client = new FileClient
  val Parser: MegogoParser = new MegogoParser {
    override def Domain: String = "src/test/resource"
    override def SearchURL = s"$Domain/search-page/"
    override def searchParams(title: String) = Map(title -> ".html")
  }

  val Crawler = new Crawler(Parser, Client)

  describe("search") {

    it("если есть результат по запросу, возвращает ResponseContent") {

      val data = new RequestVideoData("Скуби-Ду", "2002")
      val result = Crawler.search(data)

      val expect =
        ResponseContent(
          "скуби-ду_2002",
          "MEGOGO",
          Seq(
            VideoItem(
              "скуби-ду_2002",
              "Скуби-Ду",
              "Покупка\nНавсегда: HD 349 руб, SD 249 руб.\nна 2 дня: HD 149 руб, SD 69 руб.",
              "src/test/resource/film-page/Скуби-Ду.html"
            )
          )
        )

      assert(result.futureValue(timeout(Span(2, Seconds))).contains(expect))

    }

    it("если нет результатов по запросу, возвращает пустое тело") {

      val data = new RequestVideoData("Скуби-Ду", "1234")
      val result = Crawler.search(data)

      assert(result.futureValue(timeout(Span(2, Seconds))).isEmpty)
    }
  }

}
