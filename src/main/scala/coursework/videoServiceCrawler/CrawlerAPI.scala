package coursework.videoServiceCrawler

import akka.http.scaladsl.server.Route
import coursework.videoServiceCrawler.crawler.CrawlerService
import coursework.videoServiceCrawler.data.RequestVideoData
//import akka.http.scaladsl.unmarshalling.Unmarshaller <-- не успела разобраться


class CrawlerAPI(crawler: CrawlerService) {

  import akka.http.scaladsl.server.Directives._
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport.marshaller


  private val crawlerFindAndSaveRoute: Route =
    (get & path("crawler" / "search") & parameter("title", "year".as[Int])) { (title, year) =>
      complete(crawler.search(RequestVideoData(title, year.toString))) //     ^  эта штука требует доработки
    }

  private val crawlerFindInStorageRoute: Route =
    (get & path("crawler" / "storage" / "search") & parameter("title", "year".as[Int]))
    { (title, year) => complete(crawler.searchInStorage(RequestVideoData(title, year.toString))) }

  val route: Route = crawlerFindAndSaveRoute ~ crawlerFindInStorageRoute
}


