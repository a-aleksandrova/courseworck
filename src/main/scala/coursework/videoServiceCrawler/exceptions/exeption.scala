package coursework.videoServiceCrawler.exceptions

sealed abstract class CrawlerException(message: String) extends Exception(message)

final case class DataNotFoundException(requestID: String)
  extends CrawlerException(s"No data with specified requestID ($requestID)")

final case class SearchErrorException()
  extends CrawlerException("No search results ):")

final case class ConnectionException(pageURL: String)
  extends CrawlerException(s"Can't connect to the $pageURL ):")
/*
   ConnectionException определенно может возникнуть, но что с ней делать - не придумала
  Было бы здорово узнать возможные варианты
 */


