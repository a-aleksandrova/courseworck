package coursework.videoServiceCrawler.exceptions

import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.server.Directives.complete
import akka.http.scaladsl.server.ExceptionHandler

object CrawlerExceptionHandler {

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  val exceptionHandler: ExceptionHandler =
    ExceptionHandler {
      case e: CrawlerException => complete(BadRequest, ExceptionResponse(e.getMessage))
    }
}
