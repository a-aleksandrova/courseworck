package coursework.videoServiceCrawler.data

/**
 * Содержит необработанные данные о соответствующих запросу фильмах/сериалах,
 * обнаруженных на странице поиска
 *
 * @param size          число найденых фильмов/сериалов
 * @param titles        названия
 * @param pricePageURLs url-адреса на страницы с условиями просмотра
 * @param contentURLs   url-адреса на страницы просмотра
 */
case class ParseResult(size: Int, titles: Seq[String], pricePageURLs: Seq[String], contentURLs: Seq[String])
