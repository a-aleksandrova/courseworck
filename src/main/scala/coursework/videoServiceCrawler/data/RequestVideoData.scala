package coursework.videoServiceCrawler.data

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}


/**
 * Данные фильма/сериала, по которым будет осуществлен его поиск
 *
 * @param title название фильма/сериала
 * @param year год выпуска фильма/сериала
 */
case class RequestVideoData(title: String, year: String) {

  /**
   * По названию и году фильма/сериала создает идентификатор для запроса
   *
   * @return идентификатор запроса созданный по названию и году фильма
   *         Пример: название_фильма_2020
   */
  def requestID: String = title.toLowerCase.split(" ").appended(year).mkString("_")
}

object RequestVideoData {
  implicit val jsonDecoder: Decoder[RequestVideoData] = deriveDecoder
  implicit val jsonEncoder: Encoder[RequestVideoData] = deriveEncoder
}
