package coursework.videoServiceCrawler.data

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}


/**
 * Содержит данные одного фильма/сериала
 *
 * @param title название
 * @param price стоимость
 * @param url ссылка на фильм/сериал
 *
 */
/* этот класс использую и для создания таблиц для всех видеосервисов в бд.
   так вообще можно?
 */
case class VideoItem(requestID: String,
                     title: String,
                     price: String,
                     url: String)

object VideoItem {
  implicit val jsonDecoder: Decoder[VideoItem] = deriveDecoder
  implicit val jsonEncoder: Encoder[VideoItem] = deriveEncoder
}