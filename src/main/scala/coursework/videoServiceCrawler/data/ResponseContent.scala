package coursework.videoServiceCrawler.data

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

/**
 * Содержит фильмы/сериалы извлеченные с конкретного видеосервиса
 *
 * @param requestID id запроса, по которому найден контент
 * @param videoService название видеосервиса
 * @param content список из фильмов/сериалов в формате VideoItem
 *
 */
case class ResponseContent(requestID: String, videoService: String, content: Seq[VideoItem])


object ResponseContent {
  implicit val jsonDecoder: Decoder[ResponseContent] = deriveDecoder
  implicit val jsonEncoder: Encoder[ResponseContent] = deriveEncoder
}


