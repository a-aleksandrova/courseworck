package coursework.videoServiceCrawler

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route

import scala.concurrent.ExecutionContext
import coursework.videoServiceCrawler.crawler.{Crawler, CrawlerService}
import coursework.videoServiceCrawler.crawler.clients.SimpleHttpClient
import coursework.videoServiceCrawler.crawler.parsers.MegogoParser
import coursework.videoServiceCrawler.exceptions.CrawlerExceptionHandler
import coursework.videoServiceCrawler.storage.h2db.h2db


object CrawlerHttpApp extends App {
  implicit val ac: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = ac.dispatcher

  val crawlers = Seq(
    new Crawler(new MegogoParser, new SimpleHttpClient)
    //new Crawler(new SomeParser, new SpecHttpClient)
    //...
  )

  val crawler = new CrawlerService(crawlers)
  val crawlerRoute: CrawlerAPI = new CrawlerAPI(crawler)

  val routes = Route.seal(crawlerRoute.route)(
    exceptionHandler = CrawlerExceptionHandler.exceptionHandler)

  h2db.init // <--- этому здесь место?
  Http()
    .newServerAt("localhost", 8080)
    .bind(routes)

}
