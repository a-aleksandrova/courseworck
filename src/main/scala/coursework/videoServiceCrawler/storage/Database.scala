package coursework.videoServiceCrawler.storage


import coursework.videoServiceCrawler.data.ResponseContent

import scala.concurrent.Future


/**
 * Хранилище данных фильмов/сериалов
 */
trait Database {


  /**
   * Достает из хранилище данные фильма/сериала, которые были найдены ранее
   *
   * @param requestID id запроса фильма/сериала, по которому ранее были найдены данные
   * @return если данные о фильме/сериале есть в хранилище -
   *         список из ResponseContent от всех видеосервисов с успешным результатом,
   *         если данных нет - пустой список
   */
  def find(requestID: String): Future[Seq[ResponseContent]]

  /**
   * Вставляет в хранилище данные фильма/сериала
   *
   * @param data данные фильма/сериала, которые требуется поместить в хранилище
   * @return эти же данные фильма/сериала <-- или не стоит возвращать их здесь
   *                                          и лучше это делать где-нибудь из CrawlerRunner?
   */
  def insert(data: ResponseContent): Future[ResponseContent]

}
