package coursework.videoServiceCrawler.storage.h2db

import java.util.UUID

import slick.jdbc.H2Profile.api._
import slick.dbio.{DBIO, DBIOAction, Effect, NoStream}
import slick.jdbc.JdbcBackend.Database

import scala.concurrent.Future


//или надо делать это штуку компаньоном непосредственно H2DataBase? Или как-то еще иначе?
object h2db {

  type DIO[+R, -E <: Effect] = DBIOAction[R, NoStream, E]
  val DIO = DBIO

  def init: Future[Unit] =  db.run(initSchema)

  val db = Database.forURL(
    s"jdbc:h2:mem:${UUID.randomUUID()}",
    driver = "org.h2.Driver",
    keepAliveConnection = true
  )

  private val initSchema =
    ContentQueryRepository.MegogoContent.schema.create
}