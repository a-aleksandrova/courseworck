package coursework.videoServiceCrawler.storage.h2db

import coursework.videoServiceCrawler.data.VideoItem
import slick.dbio.Effect
import slick.jdbc.H2Profile.api._

/* таблицы под все видеосервисы здесь оставлять,
   либо лучше каждую в отдельном файле
   и тогда и ContentQueryRepository в отдельный файл выносить?
*/
class MegogoContentTable(tag: Tag) extends Table[VideoItem](tag, "MEGOGO_CONTENT") {

  def requestID: Rep[String] = column("REQUEST_ID")
  def title: Rep[String] = column("TITLE")
  def price: Rep[String] = column("PRICE")
  def url: Rep[String] = column("URL")

  def * = (requestID, title, price, url) <> ((VideoItem.apply _).tupled, VideoItem.unapply)
}

object ContentQueryRepository {
  val MegogoContent = TableQuery[MegogoContentTable]

  val AllContent =
    Map(
      "MEGOGO" -> MegogoContent
    //"SomeVideoService" -> SomeContent,
  )

  def findContentByRequestID(videoService: String, requestID: String): DBIOAction[Seq[VideoItem], NoStream, Effect.Read] =
    AllContent(videoService)
      .filter(_.requestID === requestID).result

  def insertContent(videoService: String, data: Seq[VideoItem]): DBIOAction[Option[Int], NoStream, Effect.Write] =
    AllContent(videoService) ++= data

}