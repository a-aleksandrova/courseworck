package coursework.videoServiceCrawler.storage.h2db

import ContentQueryRepository.{findContentByRequestID, insertContent}
import coursework.videoServiceCrawler.data.ResponseContent
import coursework.videoServiceCrawler.storage.Database
import coursework.videoServiceCrawler.storage.h2db.h2db.db

import scala.concurrent.{ExecutionContext, Future}

class H2DataBase(implicit ec: ExecutionContext) extends Database{

  override def find(requestId: String): Future[Seq[ResponseContent]] = {
    val action =
      for {
        megogoContent <- findContentByRequestID("MEGOGO", requestId)
      //someContent <- findContentByRequestID("SomeVideoService", requestId)
    } yield
      Seq(
        ResponseContent(requestId, "MEGOGO", megogoContent)
       //ResponseContent(requestId, "SomeVideoService", someContent),
      )

    db.run(action.map(_.filter(_.content.nonEmpty)))
  }

  override def insert(data: ResponseContent): Future[ResponseContent] =

    db.run(insertContent(data.videoService, data.content)).map(_ => data)

}
