package coursework.videoServiceCrawler.storage

import coursework.videoServiceCrawler.data.ResponseContent
import coursework.videoServiceCrawler.exceptions.DataNotFoundException

import scala.concurrent.{ExecutionContext, Future}

/**
 * Здесь получается точно такая же документация, как и у Database,
 * только с добавлением информации о возвращаемых ошибках.
 * Что-то не так, да?
 */
class CrawlerStorage(db: Database)(implicit ec: ExecutionContext) {

  def find(requestID: String): Future[Seq[ResponseContent]] =
    db.find(requestID).flatMap{
      case Seq() => Future.failed(DataNotFoundException(requestID)) //этому исключению место здесь или в CrawlerRunner?
      case res => Future.successful(res)
    }

  def insert(data: ResponseContent): Future[ResponseContent] =
    db.insert(data)
}
