package coursework.videoServiceCrawler.crawler.clients

import java.io.File

import org.jsoup.Jsoup
import org.jsoup.nodes.Document

import scala.concurrent.{ExecutionContext, Future, blocking}

/**
 * Клиент для тестов,
 * переходит по path и забирает html-страницу из файла.
 */
class FileClient(implicit ec: ExecutionContext) extends Client {

  override def get(path: String, params: Map[String, String] = Map()): Future[Document] =
    Future{
      val p = params.map{case param->value => param+value}.mkString("")
      val htmlFile = new File(path + p)
      blocking(Jsoup.parse(htmlFile, "UTF-8"))
    }
}
