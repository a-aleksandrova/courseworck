package coursework.videoServiceCrawler.crawler.clients

import org.jsoup.nodes.Document

import scala.concurrent.Future

trait Client {

  /**
   * Переходит по url-адресу и возвращает html-документ
   *
   * @param url    url-адрес
   * @param params параметры зыпроса (опционально)
   * @return html-документ
   */
  def get(url: String, params: Map[String, String] = Map()): Future[Document]

}
