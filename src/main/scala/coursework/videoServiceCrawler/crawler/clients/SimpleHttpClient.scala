package coursework.videoServiceCrawler.crawler.clients

import java.net.SocketTimeoutException

import coursework.videoServiceCrawler.exceptions.ConnectionException
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.CollectionConverters.MapHasAsJava

/**
 * Клиент, работающий с http/https протоколом:
 * переходит по url-адрусу и забирает html-страницу.
 */
class SimpleHttpClient(implicit ec: ExecutionContext) extends Client {

  override def get(url: String, params: Map[String, String]): Future[Document] =
    Future {
      Jsoup
        .connect(s"https://$url")
        .data(params.asJava)
        .get
    }.
      recoverWith{ case ex: SocketTimeoutException =>
        Future.failed(ConnectionException(url))
      }
}
