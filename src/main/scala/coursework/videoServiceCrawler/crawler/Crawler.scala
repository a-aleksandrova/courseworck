package coursework.videoServiceCrawler.crawler

import coursework.videoServiceCrawler.crawler.clients.Client
import coursework.videoServiceCrawler.crawler.parsers.Parser
import coursework.videoServiceCrawler.data.{RequestVideoData, ResponseContent, VideoItem}
import coursework.videoServiceCrawler.exceptions.SearchErrorException
import coursework.videoServiceCrawler.storage.CrawlerStorage

import scala.concurrent.{ExecutionContext, Future}

/**
 * Позволяет реализовать краулер под определенный видео-сервис
 *
 * @param parser парсер под определенный видеосервис
 * @param client клиент для получения html-страниц с видео-сервиса
 */
class Crawler(parser: Parser, client: Client)(implicit ec: ExecutionContext)  {

  /**
   * Название видеосервиса
   */
  def videoService: String = parser.VideoService

  /**
   * Достает данные фильма/сериала с видео-сервиса
   *
   * @param data RequestVideoData по которому будет осуществлен поиск фильма/сериала
   * @return если фильм/сериал найден - ResponseContent, обернутый в Some,
   *         если не найден - None.
   */
  def search(data: RequestVideoData): Future[Option[ResponseContent]] = {
    val requestId = data.requestID
    val searchUrl = parser.SearchURL
    val params = parser.searchParams(data.title)

    for {
      searchPage <- client.get(searchUrl, params)
      searchResults <- parser.searchVideo(searchPage, data)
      numItems = searchResults.size
      titles = searchResults.titles
      videoPagesURLs = searchResults.contentURLs
      pricePageURLs = searchResults.pricePageURLs
      pricePages <- Future.traverse(pricePageURLs)(url => client.get(url))
      conditions <- Future.traverse(pricePages)(page => parser.getConditions(page))

      videoItems = 0.until(numItems).map(i => VideoItem(requestId, titles(i), conditions(i), videoPagesURLs(i)))

    } yield
      if (numItems == 0) None
      else Some(ResponseContent(requestId, videoService, videoItems))

  }

}
