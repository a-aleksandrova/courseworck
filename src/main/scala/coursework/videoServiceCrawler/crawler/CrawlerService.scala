package coursework.videoServiceCrawler.crawler

import coursework.videoServiceCrawler.storage.CrawlerStorage
import coursework.videoServiceCrawler.storage.h2db.H2DataBase
import coursework.videoServiceCrawler.data.{RequestVideoData, ResponseContent}
import coursework.videoServiceCrawler.exceptions.SearchErrorException


import scala.concurrent.{ExecutionContext, Future}


/**
 * Позволяет работать со всеми краулерами одновременно
 *
 * @param crawlers список краулеров видео-сервисов, с которыми хотим работать
 */
class CrawlerService(crawlers: Seq[Crawler])(implicit ec: ExecutionContext) {
  //private?
  val db = new H2DataBase
  val storage = new CrawlerStorage(db)

  /**
   * Находит данные фильма/сериала по запросу и помещает их в хранилище
   *
   * @param data RequestVideoData по которому будет осуществлен поиск фильма/сериала
   * @return список из ResponseContent c видеосервисов на которых обнаружен требуемый фильм/сериал,
   *         либо SearchErrorException, если фильм/сериал не обнаружен ни на одном из видеосервисов
   */
  def search(data: RequestVideoData): Future[Seq[ResponseContent]] = {
      Future
        .traverse(crawlers)(_.search(data))
        .map(_.flatten)
        .flatMap{
          case Seq() =>  Future.failed(SearchErrorException()) //или такие исключения лучше бросать из CrawlerStorage?
          case content => Future.traverse(content)(storage.insert)
        }
  }
  /* или вставлять в базу все разом (но надо бы разобраться как)

     Future.
      traverse(crawlers)(_.search(data))
      .map(_.flatten)
      .flatMap(res =>
        if (res.nonEmpty) storage.insert(res)
        else Future.failed(SearchErrorException()))
   */

  /**
   * Находит в хранилище данные всех фильмов/сериалов по запросу
   *
   * @param data RequestVideoData по которому будет осуществлен поиск фильма/сериала
   * @return если данные о фильме/сериале есть в хранилище -
   *         список из ResponseContent от всех видеосервисов с успешным результатом,
   *         если данных нет - DataNotFoundException
   */
  def searchInStorage(data: RequestVideoData): Future[Seq[ResponseContent]] =
    storage.find(data.requestID)
}
