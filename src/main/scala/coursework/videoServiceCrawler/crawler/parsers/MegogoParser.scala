package coursework.videoServiceCrawler.crawler.parsers

import coursework.videoServiceCrawler.data.{ParseResult, RequestVideoData}
import org.jsoup.nodes.Document

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.jdk.CollectionConverters.CollectionHasAsScala


class MegogoParser extends Parser {
  def VideoService = "MEGOGO"
  def Domain = "megogo.ru"
  def SearchURL = s"$Domain/ru/search-extended"
  def searchParams(title: String) = Map("q" -> title, "tab" -> "video")

  val VideoEl = "div.card"
  val TitleEl = "h3.video-title"
  val YearEl = "span.video-year"
  val PathEl = "div.thumb > a[href]"

  val PriceEl = "div.overlay-content > * div.pQuality"
  val SubscribeEl = "div.trailer-overlay > p.stub-description"
  val FreeEl = "div.videoView-headerWrapper[data-advod-adfree=true]"

  def searchVideo(doc: Document, data: RequestVideoData): Future[ParseResult] = {

    val targetTitle = titlePattern(data.title)
    val targetVideo =
      targetTitle
        .map(title => doc
          .select(s"$VideoEl:has($TitleEl:matches($title))")
          .select(s"$VideoEl:has($YearEl:matches(^${data.year}))"))

    for {
      titles <- targetVideo.map(_
        .select(TitleEl)
        .eachText()
        .asScala.toSeq)

      urls <- targetVideo.map(_
        .select(PathEl)
        .eachAttr("href")
        .asScala.toSeq)
        .flatMap(Future.traverse(_)(extractPath)) //может, теперь на две операции разбить для читабельности

      size <- targetVideo.map(_.size())

    } yield ParseResult(size, titles, urls, urls)
    // цена и фильм на одной станице    ^    ^

  }

  def getConditions(doc: Document): Future[String] = {
    // одновременно проверят страницу
    // и на покупку, и на подписку, и на бесплатность - такое себе :с
    for {
      subscribe <- Future(doc.select(SubscribeEl))
      purchase <- Future(doc.select(PriceEl))
      free <- Future(doc.select(FreeEl))
      conditions <-
        if (!subscribe.isEmpty) extractSubscribe(subscribe.text)
        else if (!purchase.isEmpty) extractPurchase(purchase.text)
        else if (!free.isEmpty) Future.successful("Бесплатно")
        else Future.successful("элемент не найден ):") //<-- exception, если html-страницу внесли изменения?
                                                       //    если такое вообще случается
    } yield conditions
  }

  // не нашла аналог (?i) для кириллицы
  private def titlePattern(title: String) = Future {
    title
      .split(" ")
      .map { w =>
        val first = w.head                                                     // первая буква слова
        if (first.isLetter) "[" + first.toUpper + first.toLower + "]" + w.tail // слово => [Сс]лово
        else w                                                                 // число
      }.mkString("^", """\s""", """(\s\(.+\))?$""")
  }

  private def extractPurchase(text: String) = Future {
    val variableResults = text.split("""\s(?=[Нн]а)""")
    val findResult = """(.*?)(?=(\s[A-Z]{2,4}))\s(.*)""".r

    variableResults
      .map{
        case findResult(time, _, prises) =>
          s"$time: " + prises.split("""\.\s""").mkString(", ")
        case _ => "Не удалось получить условия покупки):"
      }.mkString("Покупка\n", "\n", "")
  }

  private def extractSubscribe(text: String) = Future {
    val prise = """(\d{1,4})\sруб./мес""".r
    prise.findFirstIn(text) match {
      case Some(prise) => s"Подписка - $prise"
      case None => "Не удалось получить усовия подписки):"
    }
  }
  //megogo успел изменить href (добавил протокол и домен)
  private def extractPath(url: String) = Future {
    val findPath = """(https:\/\/)?(.*)""".r
    url match { case findPath(_, path) => path }

  }

}
