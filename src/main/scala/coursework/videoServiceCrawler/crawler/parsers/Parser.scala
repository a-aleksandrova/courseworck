package coursework.videoServiceCrawler.crawler.parsers

import coursework.videoServiceCrawler.data.{ParseResult, RequestVideoData}
import org.jsoup.nodes.Document

import scala.concurrent.Future

trait Parser {

  /**
   * Название видеосервиса
   */
  def VideoService: String

  /**
   * Домен видеосервиса
   */
  def Domain: String

  /**
   * url-адрес страницы поиска
   */
  def SearchURL: String

  /**
   * параметры, необходимые для поиска фильма по названию
   */
  def searchParams(title: String): Map[String, String]

  /**
   * Находит на переданной странице все версии заданного в data фильма/сериала
   *
   * @param doc  html-документ, содержащий страницу с фильмами
   * @param data RequestVideoData по которому будет осуществлен поиск фильма/сериала
   * @return ParseResult, содержащий данные фильма/сериала
   */
  def searchVideo(doc: Document, data: RequestVideoData): Future[ParseResult]

  /**
   * Находит на переданной странице условия просмотра фильма/сериала
   *
   * @param doc html-документ содержащий условия просмотра
   * @return текст условий просмотра
   */
  def getConditions(doc: Document): Future[String]


}
